= 1.5.0 (Release on 19 Oktober 2022) =
- Improve: Mengurutkan kolom alamat ongkoskirim.id di halaman checkout
- Improve: Support PHP 8

= 1.4.0 (Release on 7 Oktober 2022) =
- Fix: Menyembunyikan kolom alamat billing dari pluginongkoskirim.id saat menggunakan metode kirim ke alamat lain

= 1.3.4 (Release on 17 Oktober 2022) =
- Fix: Menghapus notifikasi error variabel pada halaman checkout

= 1.3.2 (Release on 2 Juni 2022) =
- Fix: Menghapus kolom distrik dari plugin, jika berpindah ke plugin onkir lain

= 1.3.1 (Release on 13 April 2021) =
- Fix: Kecamatan pada plugin ongkir by tonjo

= 1.2.4 (Release on 15 Oktober 2021) =
- Fix: text phone di halaman checkout

= 1.2.3 (Release on 20 September 2021) =
- Fix: textarea style

= 1.2.2 (Release on 1 September 2021) =
- Fix: js pada jika opsi shipping dihilangkan

= 1.2.1 (Release on 9 Agustus 2021) =
- Fix: mengurutkan kolom kecamatan

= 1.2.0 (Release on 19 Juli 2021) =
- New: mengurutkan kolom pada plugin pluginongkoskirim.com

= 1.1.0 (Release on 16 Juli 2021) =
- Fix: Mengurutkan kolom halaman checkout
- Fix: Sembunyikan kolom kode post secara default

= 1.0.0 Initial Released =