/*=================================================;
/* HIDDEN BILLING ADDRESS
/*================================================= */
function wfc_different_address() {
  const billings = document.querySelectorAll(".woocommerce-billing-fields .address-field, #billing_city_field, #billing_company_field");
  const shipping = document.querySelector(
    "#ship-to-different-address-checkbox"
  );

  // return false if shipping form not active
  if (shipping ==  null) {
    return false;
  }

  if (shipping.checked == true) {
    billings.forEach((billing) => {
      slideUp(billing);
    });
  }

  shipping.addEventListener("change", (event) => {
    const _this = event.currentTarget;

    // action field
    if (_this.checked == true) {
      billings.forEach((billing) => {
        slideUp(billing);
      });
    } else {
      billings.forEach((billing) => {
        if (!billing.classList.contains("is-hidden")) {
          slideDown(billing);
        }
      });
    }
  });
}
wfc_different_address();