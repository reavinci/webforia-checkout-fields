<?php

/**
 * Plugin Name: Webforia Checkout Field
 * Description: Plugin optimasi kolom pada halaman checkout
 * Plugin URI:  https://webforia.id
 * Version:    1.5.0
 * Author:     Webforia Studio
 * Author URI:  https://webforia.id
 * Text Domain: webforia-checkout-field
 */

if (!defined('ABSPATH')) {
    exit;
}

define('WEBFORIA_CHECKOUT_FIELD_TEMPLATE', plugin_dir_path(__FILE__));
define('WEBFORIA_CHECKOUT_FIELD_ASSETS', plugin_dir_url(__FILE__));
define('WEBFORIA_CHECKOUT_FIELD_DOMAIN', 'webforia-checkout-field');

/*=================================================;
/* LOAD THIS PLUGIN AFTER RETHEME LOAD
/*================================================= */
function wcf_plugin_loaded() {
    // load woocommerce if woocommerce plugin active
    if (class_exists('WooCommerce')) {

        require_once WEBFORIA_CHECKOUT_FIELD_TEMPLATE . '/functions.php';
        require_once WEBFORIA_CHECKOUT_FIELD_TEMPLATE . '/plugin.php';
        require_once WEBFORIA_CHECKOUT_FIELD_TEMPLATE . '/includes/include.php';

        // run class
        new Webforia_Checkout_Fields\Plugin_Init;
        new Webforia_Checkout_Fields\Customizer;
        new Webforia_Checkout_Fields\Checkout_Field;
        new Webforia_Checkout_Fields\Custom_Shipping_Field;
    }

    // update plugin
    $check_update = Puc_v4_Factory::buildUpdateChecker('https://gitlab.com/rereyossi/webforia-field-checkout', __FILE__, 'webforia-checkout-field');
    $check_update->setBranch('stable_release');

    // call back after this plugin loaded
    do_action('wcf_after_plugin_loaded');
}

add_action('rt_after_setup_theme', 'wcf_plugin_loaded');
