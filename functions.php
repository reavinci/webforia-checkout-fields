<?php
function wcf_get_option($args, $default = '')
{
    if (function_exists('rt_option')) {
        return rt_option($args, $default);
    }

    if (function_exists('rt_get_option')) {
        return rt_get_option($args, $default);
    }
}
