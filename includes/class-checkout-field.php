<?php

namespace Webforia_Checkout_Fields;

/**
 *
 */
class Checkout_Field {

    public function __construct() {
        add_filter('woocommerce_form_field_args', [$this, 'form_style'], 10, 3);
        add_filter('woocommerce_form_field_args', [$this, 'account_fields'], 10, 3);
        add_filter('woocommerce_default_address_fields', [$this, 'address_fields'], 99);

        add_filter('woocommerce_default_address_fields', [$this, 'field_fullname'], 99);

        add_filter('woocommerce_billing_fields', [$this, 'default_account_address']);
        add_action('woocommerce_checkout_process', [$this, 'different_address']);
        add_action('woocommerce_checkout_order_processed', [$this, 'split_fullname'], 10, 1);
        add_action('init', [$this, 'remove_notes']);

        add_filter('pok_fields_priority', [$this, 'pluginongkoskirim_fields_priority'], 99);
    }


    /**
     * Plugin onkir by tonjo filter priority
     *
     * @see classes/class-pok-hooks-addresses.php
     * @hook filter pok_fields_priority
     * @param [type] $priority
     */
    public function pluginongkoskirim_fields_priority($priority) {

        return [
            'first_name' => 10,
            'last_name' => 20,
            'email' => 30,
            'phone' => 40,
            'company' => 50,
            'country' => 60,
            'state' => 70,
            'city' => 80,
            'district' => 90,
            'simple_address' => 90,
            'address_1' => 100,
            'address_2' => 110,
            'postcode' => 120,
            'insurance' => 9999,
        ];
    }

    /**
     * Get plugin support district
     */
    public function get_district() {
        $field = false;

        if (function_exists('run_ongkoskirim_id') || class_exists('Plugin_Ongkos_Kirim')) {
            $field = true;
        }

        return apply_filters('wcd_field_district', $field);
    }

    /**
     * Plugin change address 2 to district field
     */
    public function get_address_2_district() {
        $field = false;

        if (function_exists('woongkir_autoload')) {

            $field = true;
        }

        return apply_filters('wcd_field_address_2_district', $field);
    }

    /**
     * Add overlay style on checkout form
     *
     * @param [type] $args
     * @param [type] $key
     * @param [type] $value
     * @hook filter woocommerce_form_field_args
     * @return void
     */
    public function form_style($args, $key, $value = null) {

        if (is_checkout() && !in_array($args['type'], ['checkbox', 'radio'])) {

            // add class field overlay on checkout page
            $args['class'][] = 'rt-form--overlay js-form-overlay';

            if ($args['type'] == 'select') {
                $args['class'][] = 'rt-form--select2';
            }

            if ($args['type'] == 'textarea') {
                $args['class'][] = 'rt-form--textarea';
            }
        }

        return $args;
    }

    /*
     * Account field editor
     *
     * @hook filter woocommerce_form_field_args
     */
    public function account_fields($args, $key, $value = null) {
        $fields_district = [];

        if ($this->get_district()) {
            $fields_district = [
                'district' => [
                    'status' => wcf_get_option('woocommerce_checkout_field_city', 'required'),
                    'priority' => 90,
                ],
            ];
        }

        $fields = wp_parse_args($fields_district, [
            'email' => [
                'status' => wcf_get_option('woocommerce_checkout_field_email', 'required'),
                'priority' => 30,
                'placeholder' => __('john.doe@email.com', RT_THEME_DOMAIN),
            ],
            'phone' => [
                'status' => get_option('woocommerce_checkout_phone_field'),
                'priority' => 40,
            ],

        ]);

        foreach ($fields as $field => $value) {

            $status = !empty($value['status']) ? $value['status'] : '';
            $priority = !empty($value['priority']) ? $value['priority'] : '';

            if ($args['id'] == "billing_{$field}" || $args['id'] == "shipping_{$field}") {

                // Optional
                if ($status == 'optional') {
                    $args['required'] = false;
                }

                // Required
                if ($status == 'required') {
                    $args['required'] = true;
                }

                // Remove
                if ($status == 'hidden') {
                    $args['class'][] = 'is-hidden';
                    $args['required'] = false;
                }

                // Email
                if ($args['type'] == 'email') {
                    $args['placeholder'] = __('john.doe@email.com', RT_THEME_DOMAIN);
                }

                // Priority
                $args['priority'] = $priority;
            }
        }

        if ($args['id'] == "billing_district" || $args['id'] == "shipping_district") {
            $args['priority'] = $priority;
        }

        return $args;
    }

    /**
     * Address checkout editor
     *
     * @hook filter woocommerce_default_address_fields
     */
    public function address_fields($address_fields) {
        $fields = array(
            'first_name' => [
                'status' => wcf_get_option('woocommerce_checkout_field_name', 'fullname'),
                'priority' => 10,
            ],
            'last_name' => [
                'status' => wcf_get_option('woocommerce_checkout_field_name', 'fullname'),
                'priority' => 20,
            ],
            'company' => [
                'status' => get_option('woocommerce_checkout_company_field'),
                'priority' => 50,
            ],
            'country' => [
                'status' => wcf_get_option('woocommerce_checkout_field_country', 'hidden'),
                'priority' => 60,
            ],
            'state' => [
                'status' => wcf_get_option('woocommerce_checkout_field_state', 'required'),
                'priority' => 70,
            ],
            'city' => [
                'status' => wcf_get_option('woocommerce_checkout_field_city', 'required'),
                'priority' => 80,
            ],
            'address_1' => [
                'status' => wcf_get_option('woocommerce_checkout_field_address_1', 'required'),
                'priority' => 100,
            ],
            'address_2' => [
                'status' => get_option('woocommerce_checkout_address_2_field'),
                'priority' => 110,
            ],
            'postcode' => [
                'status' => wcf_get_option('woocommerce_checkout_field_postcode', 'hidden'),
                'priority' => 120,
            ],


        );

        foreach ($fields as $key => $field) {
            $status = !empty($field['status']) ? $field['status'] : '';
            $priority = !empty($field['priority']) ? $field['priority'] : '';

            // Optional
            if ($status == 'optional') {
                $address_fields[$key]['required'] = false;
            }

            // Required
            if ($status == 'required') {
                $address_fields[$key]['required'] = true;
            }

            // Remove
            if ($status == 'hidden') {
                $address_fields[$key]['class'][] = 'is-hidden';
                $address_fields[$key]['required'] = false;
            }

            // Select2
            if ($key == "address_2" && $this->get_address_2_district() || in_array($key, ['country', 'state', 'city', 'district'])) {
                $address_fields[$key]['class'][] = 'rt-form--select2';
            }

            // Remove order for all field
            unset($address_fields[$key]['priority']);

            // Set priority
            $address_fields[$key]['priority'] = $priority;
        }

        return $address_fields;
    }

    /**
     * Fullname on checkout page
     *

     * Replace first name to fullname
     * remove last name field
     *
     * @hook filter woocommerce_default_address_fields
     * @return void
     */
    public function field_fullname($fields) {
        // Full name
        // replace first name to fullname
        // remove last name

        if (wcf_get_option('woocommerce_checkout_field_name', 'fullname') == 'fullname' && is_checkout()) {
            $fields['first_name']['label'] = __('Full Name', RT_THEME_DOMAIN);
            $fields['first_name']['class'][] = 'rt-form-fullname';
            $fields['first_name']['placeholder'] = __('John Doe', RT_THEME_DOMAIN);

            $fields['last_name']['class'][] = 'is-hidden';
            $fields['last_name']['required'] = false;
        }

        return $fields;
    }

    /**
     * Split fullname from billing and shipping to first name and last name
     *
     * Split fullname if button checkout process
     * Save first name and last name from full name to database address
     *
     * @param [type] $order_id
     * @hook action woocommerce_checkout_order_processed
     * @return void
     */
    public function split_fullname($order_id) {
        if (wcf_get_option('woocommerce_checkout_field_name', 'fullname') == 'fullname') {
            $order = wc_get_order($order_id);

            $billing_first_name = get_post_meta($order_id, '_billing_first_name', true);
            $shipping_first_name = get_post_meta($order_id, '_shipping_first_name', true);

            $billing_full_name = explode(' ', $billing_first_name);
            $shipping_full_name = explode(' ', $shipping_first_name);

            // save first name
            update_post_meta($order_id, '_billing_first_name', $billing_full_name[0]);
            update_post_meta($order_id, '_shipping_first_name', $shipping_full_name[0]);

            // remove first array
            array_shift($billing_full_name);
            array_shift($shipping_full_name);

            // save last name
            update_post_meta($order_id, '_billing_last_name', implode(" ", $billing_full_name));
            update_post_meta($order_id, '_shipping_last_name', implode(" ", $shipping_full_name));
        }
    }

    /**
     * Remove note filed checkout
     *
     * @hook action init
     * @return void
     */
    public function remove_notes() {
        if (wcf_get_option('woocommerce_checkout_field_notes', 'optional') == 'hidden') {
            add_filter('woocommerce_enable_order_notes_field', '__return_false');
        }
    }

    /**
     * Set default address if user login
     *
     * Auto fill checkout field with user data from user account
     * @param [type] $fields array field
     * @hook filter woocommerce_billing_fields
     * @return void
     */
    public function default_account_address($fields) {
        if (is_user_logged_in()) {

            $user = wp_get_current_user();
            $userID = $user->ID;

            $billing_district = [];
            if ($this->get_district()) {
                $billing_district = ['district'];
            }

            $billings = wp_parse_args($billing_district, [
                'first_name',
                'last_name',
                'phone',
                'email',
                'country',
                'city',
                'state',
                'address_1',
                'address_2',
                'postcode',
            ]);

            foreach ($billings as $key => $billing) {
                if (!empty($billings)) {
                    $user_billing = get_user_meta($userID, "billing_{$billing}", true);
                    $user_first_name = get_user_meta($userID, "billing_first_name", true);
                    $user_last_name = get_user_meta($userID, "billing_last_name", true);

                    if (!empty($user_billing)) {
                        if ($billing == 'first_name') {
                            $fields["billing_first_name"]['default'] = "{$user_first_name} {$user_last_name}";
                        } else {
                            $fields["billing_{$billing}"]['default'] = $user_billing;
                        }
                    }
                }
            }
        }

        return $fields;
    }

    /**
     * Handle ship to different address
     *
     * If shipping different address checked, billing address opsional not required and billing address hidden by js
     * @hook action woocommerce_checkout_process
     * @return void
     */
    public function different_address() {

        // set empty billing
        if (!empty($_POST['ship_to_different_address']) && !wc_ship_to_billing_address_only()) {

            add_filter('woocommerce_checkout_fields', function ($fields) {

                $fields['billing']['billing_simple_address']['required'] = false;
                $fields['billing']['billing_country']['required'] = false;
                $fields['billing']['billing_state']['required'] = false;
                $fields['billing']['billing_city']['required'] = false;
                $fields['billing']['billing_district']['required'] = false;
                $fields['billing']['billing_company']['required'] = false;
                $fields['billing']['billing_address_1']['required'] = false;
                $fields['billing']['billing_address_2']['required'] = false;
                $fields['billing']['billing_postcode']['required'] = false;

                return $fields;
            }, 20, 1);
        }

        // PHP < 8
        // if (!empty($_POST['ship_to_different_address']) && !wc_ship_to_billing_address_only()) {
        //     foreach ($billings as $key => $billing) {
        //         add_filter("woocommerce_process_checkout_field_{$billing}", '');
        //     }
        // }
    }
}
