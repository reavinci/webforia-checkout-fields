<?php
namespace Webforia_Checkout_Fields;

/**
 * Custom Shipping Field
 *
 * add new phone custom field on checkout page
 * @version 1.0.0
 * @since 1.0.0
 */
class Custom_Shipping_Field
{

    public function __construct()
    {
        add_filter('woocommerce_checkout_fields', [$this, 'add_phone_field']);
        add_filter('woocommerce_admin_shipping_fields', [$this, 'admin_shipping_field']);
        add_filter('woocommerce_order_formatted_shipping_address', [$this, 'order_formatted_shipping_address'], 10, 2);
    }

    /*
     * Added phone shipping field
     *
     * added new field on checkout page
     * @hook filter woocommerce_checkout_fields
     */
    public function add_phone_field($fields)
    {
        $fields['shipping']['shipping_phone'] = array(
            'label' => __('Phone', 'woocommerce'),
            'required' => false,
            'class' => array('form-row-wide'),
            'priority' => 25,
        );
        return $fields;
    }

    /*
     * Added custom phone shipping on order detail admin page
     * this field can edit and save on admin
     *
     * @hook filter woocommerce_admin_shipping_fields
     */
    public function admin_shipping_field($fields)
    {
        $fields['phone'] = array(
            'label' => __('Phone', 'woocommerce'),
            'class' => 'form-field-wide',
        );

        return $fields;
    }

    /**
     * Added phone value to address formatted after last name
     *
     * @param [type] $address address format
     * @param [type] $order
     * @hook action woocommerce_order_formatted_shipping_address
     * @return void
     */
    public function order_formatted_shipping_address($address, $order)
    {
        $billing_phone = get_post_meta($order->id, '_billing_phone', true);
        $shipping_phone = get_post_meta($order->id, '_shipping_phone', true);
        $phone = !empty($shipping_phone)?$shipping_phone:$billing_phone ;
        $last_name = get_post_meta($order->id, '_shipping_last_name', true);

        if (!empty($phone)) {
            $address['last_name'] = "{$last_name} (phone: {$phone})";
        }

        return $address;
    }

}
