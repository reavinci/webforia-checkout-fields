<?php
namespace Webforia_Checkout_Fields;

use Retheme\Customizer_Base;

class Customizer extends Customizer_Base
{
    public function __construct()
    {
        \Kirki::add_config('webforia_checkout_field_customizer', array(
            'capability' => 'edit_theme_options',
            'option_type' => 'theme_mod',
        ));

        $this->add_checkout();

    }


    public function add_checkout()
    {
        $section = 'woocommerce_checkout';

        $this->add_field(array(
            'type' => 'select',
            'settings' => 'woocommerce_checkout_field_name',
            'section' => $section,
            'label' => __('Name', WEBFORIA_CHECKOUT_FIELD_DOMAIN),
            'default' => 'fullname',
            'priority' => 1,
            'choices' => array(
                'fullname' => 'Full Name',
                'dual-name' => 'First Name & Last Name',
            ),
        ));

        $this->add_field(array(
            'type' => 'select',
            'settings' => 'woocommerce_checkout_field_email',
            'section' => $section,
            'label' => __('Email', WEBFORIA_CHECKOUT_FIELD_DOMAIN),
            'default' => 'required',
            'priority' => 1,
            'choices' => array(
                'hidden' => 'Hidden',
                'optional' => 'Optional',
                'required' => 'Required',
            ),
        ));
        $this->add_field(array(
            'type' => 'select',
            'settings' => 'woocommerce_checkout_field_postcode',
            'section' => $section,
            'label' => __('Post Code', WEBFORIA_CHECKOUT_FIELD_DOMAIN),
            'default' => 'hidden',
            'priority' => 1,
            'choices' => array(
                'hidden' => 'Hidden',
                'optional' => 'Optional',
                'required' => 'Required',
            ),
        ));
        $this->add_field(array(
            'type' => 'select',
            'settings' => 'woocommerce_checkout_field_notes',
            'section' => $section,
            'label' => __('Notes', WEBFORIA_CHECKOUT_FIELD_DOMAIN),
            'default' => 'optional',
            'priority' => 2,
            'choices' => array(
                'hidden' => 'Hidden',
                'optional' => 'Optional',
            ),
        ));

        $this->add_field(array(
            'type' => 'select',
            'settings' => 'woocommerce_checkout_field_country',
            'section' => $section,
            'label' => __('Country', WEBFORIA_CHECKOUT_FIELD_DOMAIN),
            'priority' => 7,
            'default' => 'hidden',
            'choices' => array(
                'hidden' => 'Hidden',
                'required' => 'Required',
            ),
        ));

        $this->add_field(array(
            'type' => 'select',
            'settings' => 'woocommerce_checkout_field_state',
            'section' => $section,
            'label' => __('Province', WEBFORIA_CHECKOUT_FIELD_DOMAIN),
            'priority' => 7,
            'default' => 'required',
            'choices' => array(
                'hidden' => 'Hidden',
                'optional' => 'Optional',
                'required' => 'Required',
            ),
        ));

        $this->add_field(array(
            'type' => 'select',
            'settings' => 'woocommerce_checkout_field_city',
            'section' => $section,
            'label' => __('City', WEBFORIA_CHECKOUT_FIELD_DOMAIN),
            'priority' => 8,
            'default' => 'required',
            'choices' => array(
                'hidden' => 'Hidden',
                'optional' => 'Optional',
                'required' => 'Required',
            ),
        ));

        $this->add_field(array(
            'type' => 'select',
            'settings' => 'woocommerce_checkout_field_address_1',
            'section' => $section,
            'label' => __('Address line 1 field', WEBFORIA_CHECKOUT_FIELD_DOMAIN),
            'default' => 'required',
            'priority' => 9,
            'choices' => array(
                'hidden' => 'Hidden',
                'optional' => 'Optional',
                'required' => 'Required',
            ),
        ));

    }
}
